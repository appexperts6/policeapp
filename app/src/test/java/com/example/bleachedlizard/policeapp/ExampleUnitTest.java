package com.example.bleachedlizard.policeapp;

import android.content.Context;

import com.example.bleachedlizard.policeapp.additionalinfofragment.AdditionalInfoContract;
import com.example.bleachedlizard.policeapp.mapfragment.MapContract;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

/**
 * CountyCoords local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    private AdditionalInfoContract.PresenterContract presenter;
    @Mock
    private AdditionalInfoContract.ViewContract view;

    private MapContract.PresenterContract mapPresenter;
    @Mock
    private MapContract.ViewContract mapView;
    @Mock
    private Context context;

    /*@Before
    public void setup() throws Exception {
        presenter = new AdditionalInfoPresenter(view, context);
        mapPresenter = new MapPresenter(mapView);

    }*/
    @Test
    public void testPresenter() throws Exception {
        //presenter.start();
        //verify(view).setPresenter(presenter);
        mapPresenter.start();
        verify(mapView).setPresenter(mapPresenter);
    }
}