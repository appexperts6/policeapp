package com.example.bleachedlizard.policeapp;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.widget.EditText;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.bleachedlizard.policeapp.mainactivity.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void selectCrimeStatsItem() throws Exception {
        onView(withId(R.id.mapView)).perform(swipeUp());
        onView(withId(R.id.mapView)).perform(swipeDown());
        onView(withId(R.id.mapView)).perform(swipeLeft());
        onView(withId(R.id.mapView)).perform(swipeRight());

    }
    /*@Test
    public void selectContactInfoItem() throws Exception {
        onView(withId(R.id.sliding_up_panel)).perform(swipeUp());
        onView(withId(R.id.sliding_up_panel)).perform(swipeDown());
    }*/

    @Test
    public void selectForce() throws Exception {
        onView(withId(R.id.drawer_layout)).perform(open());

        RecyclerView recyclerView = (RecyclerView) mActivityRule.getActivity().findViewById(R.id.recyclerview);
        int count = recyclerView.getAdapter().getItemCount();
        for (int i = 0; i < count; i++)
        onView(withId(R.id.recyclerview)).perform(RecyclerViewActions.actionOnItemAtPosition(i, click()));

    }
    @Test
    public void clickLocationBtn() throws Exception {
        onView(withId(R.id.action_location)).perform(click());
        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {

        }
    }

    @Test
    public void testSearchQuery() throws Exception {
        onView(withId(R.id.action_search)).perform(click());
        onView(isAssignableFrom(EditText.class)).perform(typeTextIntoFocusedView("tEsT"), pressKey(KeyEvent.KEYCODE_ENTER));
        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {

        }
    }
    @Test
    public void testSwitch() throws Exception {
        onView(withId(R.id.action_switch)).perform();
    }



}
