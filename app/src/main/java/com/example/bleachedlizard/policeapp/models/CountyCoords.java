package com.example.bleachedlizard.policeapp.models;





import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class CountyCoords {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("features")
    @Expose
    private List<Feature> features = new ArrayList<Feature>();

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The features
     */
    public List<Feature> getFeatures() {
        return features;
    }

    /**
     * @param features The features
     */
    public void setFeatures(List<Feature> features) {
        this.features = features;
    }




    public class Feature {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("geometry")
        @Expose
        private Geometry geometry;
        @SerializedName("properties")
        @Expose
        private Properties properties;

        /**
         * @return The type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The geometry
         */
        public Geometry getGeometry() {
            return geometry;
        }

        /**
         * @param geometry The geometry
         */
        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }

        /**
         * @return The properties
         */
        public Properties getProperties() {
            return properties;
        }

        /**
         * @param properties The properties
         */
        public void setProperties(Properties properties) {
            this.properties = properties;
        }

    }

    public class Geometry {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("coordinates")
        @Expose
        private List<List<List<Double>>> coordinates = new ArrayList<List<List<Double>>>();

        /**
         * @return The type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The coordinates
         */
        public List<List<List<Double>>> getCoordinates() {
            return coordinates;
        }

        /**
         * @param coordinates The coordinates
         */
        public void setCoordinates(List<List<List<Double>>> coordinates) {
            this.coordinates = coordinates;
        }

    }

    public class Properties {

        @SerializedName("stroke-width")
        @Expose
        private Integer strokeWidth;
        @SerializedName("fill")
        @Expose
        private String fill;
        @SerializedName("fill-opacity")
        @Expose
        private Double fillOpacity;

        /**
         * @return The strokeWidth
         */
        public Integer getStrokeWidth() {
            return strokeWidth;
        }

        /**
         * @param strokeWidth The stroke-width
         */
        public void setStrokeWidth(Integer strokeWidth) {
            this.strokeWidth = strokeWidth;
        }

        /**
         * @return The fill
         */
        public String getFill() {
            return fill;
        }

        /**
         * @param fill The fill
         */
        public void setFill(String fill) {
            this.fill = fill;
        }

        /**
         * @return The fillOpacity
         */
        public Double getFillOpacity() {
            return fillOpacity;
        }

        /**
         * @param fillOpacity The fill-opacity
         */
        public void setFillOpacity(Double fillOpacity) {
            this.fillOpacity = fillOpacity;
        }

    }
}