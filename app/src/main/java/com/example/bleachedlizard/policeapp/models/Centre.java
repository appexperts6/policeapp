package com.example.bleachedlizard.policeapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by TheAppExperts on 01/12/2016.
 */

public class Centre {

    @SerializedName("latitude")
    @Expose
    private Double Latitude;
    @SerializedName("longitude")
    @Expose
    private Double Longitude;

    public Double getLatitude() {
        return Latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }
}
