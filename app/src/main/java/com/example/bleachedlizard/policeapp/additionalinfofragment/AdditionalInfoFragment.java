package com.example.bleachedlizard.policeapp.additionalinfofragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bleachedlizard.policeapp.BaseFragment;
import com.example.bleachedlizard.policeapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdditionalInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdditionalInfoFragment extends BaseFragment implements AdditionalInfoContract.ViewContract {

    private static final String FORCE_ID = "forceId";
    private static final String NEIGHBOURHOOD_ID = "neighbourhoodId";

    private String forceId;
    private String neighbourhoodId;

    private AdditionalInfoContract.PresenterContract presenter;

    @BindView(R.id.contactInfo_recycler)
    RecyclerView recyclerView;
    private ProgressDialog progressDialog;

    public AdditionalInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param forceId Parameter 1.
     * @param neighbourhoodId Parameter 2.
     * @return A new instance of fragment AdditionalInfoFragment.
     */
    public static AdditionalInfoFragment newInstance(String forceId, String neighbourhoodId) {
        AdditionalInfoFragment fragment = new AdditionalInfoFragment();
        Bundle args = new Bundle();
        args.putString(FORCE_ID, forceId);
        args.putString(NEIGHBOURHOOD_ID, neighbourhoodId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            forceId = getArguments().getString(FORCE_ID);
            neighbourhoodId = getArguments().getString(NEIGHBOURHOOD_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.additional_info_fragment, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new AdditionalInfoPresenter(this, getContext());
        presenter.start();
        presenter.getContactDetailsList(forceId, neighbourhoodId);
        startProgressDialog();
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading contact details");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setContactDetailsAdapter(AdditionalInfoAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void displayError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void setPresenter(AdditionalInfoContract.PresenterContract presenter) {

    }
}
