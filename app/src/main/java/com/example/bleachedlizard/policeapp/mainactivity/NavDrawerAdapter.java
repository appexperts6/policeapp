package com.example.bleachedlizard.policeapp.mainactivity;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.bleachedlizard.policeapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by bleac on 21/11/2016.
 */

public class NavDrawerAdapter extends ExpandableRecyclerAdapter<NavDrawerAdapter.AdapterForce, NavDrawerAdapter.Neighbourhood,
        NavDrawerAdapter.ForceViewHolder, NavDrawerAdapter.NeighbourhoodViewHolder> {

    private Context context;
    private List<AdapterForce> forces;
    private NeighbourhoodClickListener listener;

    public NavDrawerAdapter(Context context, List<AdapterForce> forces, NeighbourhoodClickListener listener){
        super(forces);
        this.context = context;
        this.forces = forces;
        this.listener = listener;
    }



    @NonNull
    @Override
    public ForceViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View v = LayoutInflater.from(parentViewGroup.getContext()).inflate(R.layout.main_nav_parent_item, parentViewGroup, false);
        return new ForceViewHolder(v);
    }

    @NonNull
    @Override
    public NeighbourhoodViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View v = LayoutInflater.from(childViewGroup.getContext()).inflate(R.layout.main_nav_child_item, childViewGroup, false);
        return new NeighbourhoodViewHolder(v);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ForceViewHolder parentViewHolder, int parentPosition,
                                       @NonNull AdapterForce parent) {
        parentViewHolder.forceNameTextView.setText(parent.forceName);
    }

    @Override
    public void onBindChildViewHolder(@NonNull NeighbourhoodViewHolder childViewHolder, int parentPosition,
                                      int childPosition, @NonNull Neighbourhood child) {
        childViewHolder.neighbourhoodNameTextView.setText(child.name);
        childViewHolder.forceId = forces.get(parentPosition).forceId;
        childViewHolder.neighbourhoodId = child.id;
    }


    public class ForceViewHolder extends ParentViewHolder {

        @BindView(R.id.force)
        public TextView forceNameTextView;

        public ForceViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    public class NeighbourhoodViewHolder extends ChildViewHolder implements View.OnClickListener {

        @BindView(R.id.neighbourhood)
        public TextView neighbourhoodNameTextView;
        public String forceId;
        public String neighbourhoodId;

        public NeighbourhoodViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onNeighbourhoodClick(forceId, neighbourhoodId);
        }
    }

    public static class AdapterForce implements Parent<Neighbourhood> {

        String forceName;
        String forceId;
        List<Neighbourhood> neighbourhoodList;

        public AdapterForce(String forceName, String forceId, List<Neighbourhood> neighbourhoodList){
            this.forceName = forceName;
            this.forceId = forceId;
            this.neighbourhoodList = neighbourhoodList;
        }

        @Override
        public List getChildList() {
            return neighbourhoodList;
        }

        @Override
        public boolean isInitiallyExpanded() {
            return false;
        }
    }

    public static class Neighbourhood {

        public String name;
        public String id;

        public Neighbourhood(String name, String id){
            this.name = name;
            this.id = id;
        }
    }

    public interface NeighbourhoodClickListener {
        void onNeighbourhoodClick(String forceId, String neighbourhoodId);
    }
}
