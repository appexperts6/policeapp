package com.example.bleachedlizard.policeapp.models;


        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


public class Contact {

    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("contact_details")
    @Expose
    private ContactDetails contactDetails;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("rank")
    @Expose
    private String rank;

    /**
     *
     * @return
     * The bio
     */
    public String getBio() {
        return bio;
    }

    /**
     *
     * @param bio
     * The bio
     */
    public void setBio(String bio) {
        this.bio = bio;
    }

    /**
     *
     * @return
     * The contactDetails
     */
    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    /**
     *
     * @param contactDetails
     * The contact_details
     */
    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The rank
     */
    public String getRank() {
        return rank;
    }

    /**
     *
     * @param rank
     * The rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

}
