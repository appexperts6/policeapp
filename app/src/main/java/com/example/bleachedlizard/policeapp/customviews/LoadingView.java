package com.example.bleachedlizard.policeapp.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by PC-TAE on 02/12/2016.
 */

public class LoadingView extends ProgressBar {
    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    Runnable load = new Runnable() {
        @Override
        public void run() {
            setProgress(getProgress()+(getMax()/10));
            if (getProgress()>=getMax()){
                setProgress(0);
            }
            setAlpha(getAlpha()-0.01f);
            invalidate();
        }
    };

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        load.run();
    }
}
