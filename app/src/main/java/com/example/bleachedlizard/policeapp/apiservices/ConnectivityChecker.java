package com.example.bleachedlizard.policeapp.apiservices;

/**
 * Created by bleac on 21/11/2016.
 */

import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ConnectivityChecker {

    public void checkInternetConnectivity(final ConnectivityCheckListener listener) {
        ReactiveNetwork.observeInternetConnectivity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        listener.onConnectivityChecked(aBoolean);
                    }
                });
    }


    public interface ConnectivityCheckListener {
        void onConnectivityChecked(boolean isConnectedToInternet);
    }
}