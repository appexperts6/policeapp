package com.example.bleachedlizard.policeapp.additionalinfofragment;

import android.content.Context;
import android.util.Log;

import com.example.bleachedlizard.policeapp.apiservices.ConnectionService;
import com.example.bleachedlizard.policeapp.apiservices.ConnectivityChecker;
import com.example.bleachedlizard.policeapp.apiservices.CrimeStatsApi;
import com.example.bleachedlizard.policeapp.models.Contact;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by bleac on 21/11/2016.
 */

public class AdditionalInfoPresenter implements AdditionalInfoContract.PresenterContract,
        ConnectivityChecker.ConnectivityCheckListener {

    private AdditionalInfoContract.ViewContract fragment;
    private Context context;
    private String forceId;
    private String neighbourhoodId;
    private CrimeStatsApi observables;

    public AdditionalInfoPresenter(AdditionalInfoContract.ViewContract fragment, Context context){
        this.fragment = fragment;
        this.context = context;
    }

    @Override
    public void start() {
        connectToApi();
    }

    private void connectToApi(){
        observables = ConnectionService.getClient().create(CrimeStatsApi.class);
    }

    public void getContactDetailsList(String forceId, String neighbourhoodId) {
        //TODO Request contact details list from API classes
        this.forceId = forceId;
        this.neighbourhoodId = neighbourhoodId;
        new ConnectivityChecker().checkInternetConnectivity(this);
    }

    @Override
    public void onConnectivityChecked(boolean isConnectedToInternet) {

        if (isConnectedToInternet){
            getContactDetailsFromInternet(forceId, neighbourhoodId);
        }

    }

    private void getContactDetailsFromInternet(String forceId, String neighbourhoodId) {

        Log.d(TAG, "getContactsFromInternet starting");
        Observable<List<Contact>> getContactsObservable = (Observable<List<Contact>>)
                ConnectionService.getPreparedObservable(observables.getContacts(forceId, neighbourhoodId), Contact.class, false, false);

        getContactsObservable.subscribe(new Observer<List<Contact>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                fragment.displayError("Problem downloading contact details");
                fragment.dismissProgressDialog();
            }

            @Override
            public void onNext(List<Contact> contacts) {

                processResults(contacts);

            }
        });
        Log.d(TAG, "getCategoriesFromInternet ending");

    }

    private void processResults(List<Contact> contacts) {
        ArrayList<ContactDetails> contactDetails = new ArrayList<>();
        for (Contact contact : contacts){
            ContactDetails contactFinal = new ContactDetails();
            contactFinal.name = contact.getName();
            contactFinal.phone = contact.getContactDetails().getTelephone();
            contactFinal.email = contact.getContactDetails().getEmail();
            contactFinal.bio = contact.getBio();
            contactDetails.add(contactFinal);
        }
        setContactDetailsList(contactDetails);
    }

    private void setContactDetailsList(List<ContactDetails> contactDetails){
        AdditionalInfoAdapter adapter = new AdditionalInfoAdapter(contactDetails);
        fragment.setContactDetailsAdapter(adapter);
        fragment.dismissProgressDialog();
    }
}
