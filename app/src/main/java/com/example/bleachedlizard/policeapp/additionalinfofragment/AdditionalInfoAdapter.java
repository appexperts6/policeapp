package com.example.bleachedlizard.policeapp.additionalinfofragment;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bleachedlizard.policeapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.http.HEAD;

/**
 * Created by bleac on 23/11/2016.
 */

public class AdditionalInfoAdapter extends RecyclerView.Adapter {

    private List<ContactDetails> contactDetailsList;

    public AdditionalInfoAdapter(List<ContactDetails> contactDetailsList){
        this.contactDetailsList = contactDetailsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_card, parent, false);
        return new ContactDetailsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactDetails contactDetails = contactDetailsList.get(position);
        ContactDetailsViewHolder viewHolder = (ContactDetailsViewHolder) holder;
        viewHolder.name.setText(contactDetails.name);
        String phone = (contactDetails.phone != null) ? contactDetails.phone : "N/A";
        String email =  (contactDetails.email != null) ? contactDetails.email : "N/A";
        viewHolder.phone.setText("Phone: " + phone);
        viewHolder.email.setText("Email: " + email);
        if (contactDetails.bio != null) {
            viewHolder.bio.setText(Html.fromHtml(contactDetails.bio));
        } else {
            viewHolder.bio.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return contactDetailsList.size();
    }


    public class ContactDetailsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_contactName)
        public TextView name;
        @BindView(R.id.txt_contactNumber)
        public TextView phone;
        @BindView(R.id.txt_contactEmail)
        public TextView email;
        @BindView(R.id.txt_bio)
        public TextView bio;

        public ContactDetailsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }


    }
}
