package com.example.bleachedlizard.policeapp.apiservices;

import com.example.bleachedlizard.policeapp.models.Contact;
import com.example.bleachedlizard.policeapp.models.Force;
import com.example.bleachedlizard.policeapp.models.LocatedNeigh;
import com.example.bleachedlizard.policeapp.models.Neighbourhoods;
import com.example.bleachedlizard.policeapp.models.crimes.CrimeList;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by bleac on 21/11/2016.
 */

public interface CrimeStatsApi {
    @GET("forces")
    Observable<List<Force>> getForces();

    @GET("{force_id}/neighbourhoods")
    Observable<List<Neighbourhoods>> getNeighbourhoods(@Path("force_id") String forceId);

    @GET("{force_id}/{nbh_id}/people")
    Observable<List<Contact>> getContacts(@Path("force_id") String forceId, @Path("nbh_id") String neighbourhood);

    @GET("{force_id}/{nbh_id}/boundary")
    Observable<List<LatLng>> getNeighbourhoodBorder(@Path("force_id") String forceId, @Path("nbh_id") String neighbourhood);

    @GET("locate-neighbourhood")
    Observable<LocatedNeigh> getNeighbourhoodByLocation(@Query("q") String latLong);

    @GET("{force_id}/{nbh_id}")
    Observable<Neighbourhoods> getNeighbourhoodDetails(@Path("force_id") String forceId, @Path("nbh_id") String neighbourhood);

    @GET("crimes-street/all-crime")
    Observable<List<CrimeList>> getCrimes(@Query("poly") String processedLatLng);
}
