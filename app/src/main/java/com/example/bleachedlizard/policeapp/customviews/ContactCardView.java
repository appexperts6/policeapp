package com.example.bleachedlizard.policeapp.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;

import com.example.bleachedlizard.policeapp.R;

import java.lang.ref.Reference;


/**
 * Created by Luke, The Keanu Reeves of custom views...
 * This view has to have a Text View Named txt_bio inside it to work.
 * (This custom view isn't very scalable but that is because it is for a specific purpose.)
 */
public class ContactCardView extends CardView {

    boolean firstDraw = true;
    boolean expanded = true;
    boolean change = false;
    boolean initialised = false;
    boolean drawArrow = true;
    Bitmap expandImg = BitmapFactory.decodeResource(getResources(), R.drawable.expand_arrow);
    Paint paint = new Paint();
    TextView bio;
    String bioText = "";
    float sas_stretch = 1f;
    float sas_max = 1.5f;
    String sas_stage = "stretch";

    /**
     * gets reference to the bio text
     */
    public void getBioReference(){
        bio = (TextView) findViewById(R.id.txt_bio);
        if (bio!=null){
            if (bio.getText()!=""){
                bioText = String.valueOf(bio.getText());
                //bio.setText("");
            }
        }
        if (bioText.equals("")){
            drawArrow=false;
        }
    }

    public ContactCardView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getBioReference();

                if ((!expanded)&&(!change)){
                    change = true;
                    Log.i("Expanding card view", "Expanded");
                    Expand();
                }
                else {
                    if (!change){
                        change = true;
                        Log.i("Expanding card view", "Reduced");
                        Reduce();
                    }
                }
                change = false;
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (firstDraw){
            Reduce();
            firstDraw=false;
        }
        animation.run();
        if (!expanded){
            if (drawArrow){
                canvas.drawBitmap(expandImg,(getWidth()/2)-(expandImg.getWidth()/2),getHeight()-(expandImg.getHeight())-8,null);
            }

        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        getBioReference();
        if (!initialised){
            initialised = true;
            expanded=false;
            bio.setAlpha(0f);

        }
    }

    public void SquashAndStretch(){
        switch(sas_stage){
            case "stretch":{
                sas_stretch+=sas_stretch+(getDifference(sas_stretch,sas_max)/5);
                if (sas_stretch>=sas_max){
                    sas_stage = "shrink";
                }
                break;
            }
            case "shrink":{
                sas_stretch-=sas_stretch+(getDifference(sas_stretch,1)/5);
                if (sas_stretch<=1f){
                    sas_stage = "end";
                    sas_stretch=1f;
                }
                break;
            }
        }
        Log.i("Squash and Stretch", sas_stage);
        Log.i("Stretched/Squashed", String.valueOf(getScaleY()));
        setScaleY(sas_stretch);
    }

    public float getDifference(float a, float b){
        if (a>b){
            return a-b;
        }
        else {
            return b-a;
        }
    }

    Runnable animation = new Runnable() {
        @Override
        public void run() {
            //SquashAndStretch();
            if (bio.getAlpha()<1){
                bio.setAlpha(bio.getAlpha()+0.01f);
                invalidate();
            }
        }
    };

    public void Expand(){
        sas_stage = "stretch";
        expanded = true;
        bio.setText(bioText);
    }

    public void Reduce(){
        expanded = false;
        bio.setText("");
        bio.setAlpha(0f);
    }
}
