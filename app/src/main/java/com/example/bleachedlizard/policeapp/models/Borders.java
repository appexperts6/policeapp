package com.example.bleachedlizard.policeapp.models;




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Borders {

@SerializedName("latitude")
@Expose
private Float latitude;
@SerializedName("longitude")
@Expose
private Float longitude;

/**
* 
* @return
* The latitude
*/
public Float getLatitude() {
return latitude;
}

/**
* 
* @param latitude
* The latitude
*/
public void setLatitude(Float latitude) {
this.latitude = latitude;
}

/**
* 
* @return
* The longitude
*/
public Float getLongitude() {
return longitude;
}

/**
* 
* @param longitude
* The longitude
*/
public void setLongitude(Float longitude) {
this.longitude = longitude;
}

}