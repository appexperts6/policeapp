package com.example.bleachedlizard.policeapp.database;

/**
 * Created by luke on 21/11/2016.
 */

public class Crime {

    public Long _id; // for cupboard
    public String crime_category; // crime category
    public String persistent_id; // crime id
    public String month; // month of the crime
    public String location; // location of the incident
    public String latitude; // Latitude
    public String street; // street the incident occured on
    public String streetid; // identifier for the street
    public String name; // name of the location
    public String longitude; // longitude
    public String context; // extra information about the crime
    public String id; // id of the crime
    public String location_type; // Type of location. AdapterForce of BTP. AdapterForce = normal police, BTP(British Transport police)
    public String location_subtype; // For BTP locations, the type of location at which this crime was recorded
    public String outcome_status; // For BTP locations, the type of location at which this crime was recorded
    public String category; //Category of the outcome
    public String date; //Date of the outcome

    public Crime(String date, String category, String persistent_id, String month, String location, String latitude, String street, String streetid, String name, String longitude, String context, String id, String location_type, String location_subtype, String outcome_status, String category1, Long _id) {
        this.date = date;
        this.crime_category = category;
        this.persistent_id = persistent_id;
        this.month = month;
        this.location = location;
        this.latitude = latitude;
        this.street = street;
        this.streetid = streetid;
        this.name = name;
        this.longitude = longitude;
        this.context = context;
        this.id = id;
        this.location_type = location_type;
        this.location_subtype = location_subtype;
        this.outcome_status = outcome_status;
        this.category = category1;
        this._id = _id;
    }
}
