package com.example.bleachedlizard.policeapp.mapfragment;

import com.example.bleachedlizard.policeapp.models.Neighbourhoods;
import com.example.bleachedlizard.policeapp.mvp.BasePresenterInterface;
import com.example.bleachedlizard.policeapp.mvp.BaseViewInterface;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by bleac on 21/11/2016.
 */

public interface MapContract {

    interface PresenterContract extends BasePresenterInterface {
        void getForces();
        void getNeighbourhoods(String force);
        void getNeighbourhoodBorders(String force,String neighbourhood);
        //shawn's code
        void getNeighbourhoodDetails(String force,String neighbourhood);


    }

    interface ViewContract extends BaseViewInterface<MapContract.PresenterContract> {
        void updateMap(List<LatLng> result ,String forceName, Boolean isNeighbourhood);
        void zoomToLocation(double lat, double lng);
        //shawn's code
        void addMarker(Neighbourhoods neighbourhoods);
    }
}

