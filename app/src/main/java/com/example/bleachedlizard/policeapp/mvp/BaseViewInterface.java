package com.example.bleachedlizard.policeapp.mvp;

/**
 * Created by bleac on 21/11/2016.
 */

public interface BaseViewInterface<T> {
    void setPresenter(T presenter);
}
