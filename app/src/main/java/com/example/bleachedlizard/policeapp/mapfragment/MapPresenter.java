package com.example.bleachedlizard.policeapp.mapfragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.bleachedlizard.policeapp.R;
import com.example.bleachedlizard.policeapp.apiservices.ConnectionService;
import com.example.bleachedlizard.policeapp.apiservices.CrimeStatsApi;
import com.example.bleachedlizard.policeapp.customviews.LoadingView;
import com.example.bleachedlizard.policeapp.models.Force;
import com.example.bleachedlizard.policeapp.models.LocatedNeigh;
import com.example.bleachedlizard.policeapp.models.Neighbourhoods;
import com.example.bleachedlizard.policeapp.models.crimes.CrimeList;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by bleac on 21/11/2016.
 */

public class MapPresenter implements MapContract.PresenterContract {
    MapContract.ViewContract ctx;
    FragmentActivity fragmentActivity;
    GoogleApiClient googleApiClient;
    MapView mapView;
    GoogleMap googleMap;
    private static String baseUrl = "https://data.police.uk/api/";
    int count = 0;
    int polygonCount = 0;
    Polygon[] polygons = new Polygon[550];
    LoadingView loadingView;
    CompositeSubscription subscription = new CompositeSubscription();


    //sweep variables
    Thread pointSweep;
    boolean sweepinit = false;
    boolean sweepComplete = false;
    String lastNeigh = "";
    float sweep_x = 0f;
    float sweep_y = 0f;
    float sweepSize = 0.05f;
    float sweepPrecision = 0.005f;
    float sweepCell;
    String sweepDir = "x+";
    boolean sweepMoved = false;
    int tick = 0;
    LatLng latLng = new LatLng(0,0);

    int crimes = 0;
    Boolean foundPoly = false;
    List<LatLng> optimisedLatLng;


    private CrimeStatsApi apiService = ConnectionService.getClient().create(CrimeStatsApi.class);
    private List<LocatedNeigh> neighs = new ArrayList<LocatedNeigh>();

    public MapPresenter(MapContract.ViewContract ctx, FragmentActivity fragmentActivity) {
        this.ctx = ctx;
        this.fragmentActivity = fragmentActivity;
        //((MapFragment)ctx).getContext()
        //Log.e("json is",loadJSONFromAsset());
    }

    @Override
    public void start() {
        ctx.setPresenter(this);
        googleApiClient = new GoogleApiClient.Builder(getContext()).addApi(LocationServices.API).build();
        googleApiClient.connect();
        loadingView = (LoadingView) fragmentActivity.findViewById(R.id.loadingView);
        loadingView.setAlpha(0);
        if (pointSweep == null) {
            pointSweep = new Thread() {
                @Override
                public void run() {
                    tick--;
                    if (!sweepinit) {
                        latLng = new LatLng(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude);
                        sweepinit = true;
                        sweepComplete = false;
                        sweepCell = sweepPrecision;
                        sweep_x = -sweepCell;
                        sweep_y = -sweepCell;
                        sweepDir = "x+";
                        subscription.remove(subscription);
                        subscription.clear();
                        loadingView.setAlpha(1);
                    }
                    if (!sweepComplete) {

                            Log.i("Sweep direction", sweepDir);
                            switch (sweepDir) {
                                case "x+": {
                                    if (!sweepMoved) {
                                        sweep_x += sweepPrecision;
                                        sweepMoved = true;
                                    }
                                    break;
                                }
                                case "y+": {
                                    if (!sweepMoved) {
                                        sweep_y += sweepPrecision;
                                        sweepMoved = true;
                                    }
                                    break;
                                }
                                case "x-": {
                                    if (!sweepMoved) {
                                        sweep_x -= sweepPrecision;
                                        sweepMoved = true;
                                    }
                                    break;
                                }
                                case "y-": {
                                    if (!sweepMoved) {
                                        sweep_y -= sweepPrecision;
                                        sweepMoved = true;
                                    }
                                    break;
                                }
                            }
                            sweepMoved = false;
                            if (sweep_x >= sweepCell) {
                                sweepDir = "y+";
                            }
                            if (sweep_y >= sweepCell) {
                                sweepDir = "x-";
                            }
                            if (sweep_x <= -sweepCell) {
                                sweepDir = "y-";
                            }
                            if (sweep_y <= -sweepCell) {
                                sweepDir = "x+";
                                if (sweepCell >= sweepSize) {
                                    sweepComplete = true;
                                    loadingView.setAlpha(0);
                                }
                                sweepCell += sweepPrecision;
                            }

                            Log.i("LATLNG", String.valueOf(latLng.latitude + sweep_x) + " " + String.valueOf(latLng.longitude + sweep_y));

                        if (!sweepComplete) {
                            Context context = getContext();
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            } else {
                                LatLng processedLatLng = new LatLng(latLng.latitude + sweep_x, latLng.longitude + sweep_y);
                                //googleMap.addMarker(new MarkerOptions().position(processedLatLng).visible(true));
                                getNeighbourhoodByLocation(latLng);
                            }

                        }

                    }
                }
            };
        }
    }




    @Override
    public void getForces() {

//        apiService.getForces(baseUrl + "forces")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<List<AdapterForce>>() {
//                    @Override
//                    public void onCompleted() {
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e("errrrrror", "" + e);
//                    }
//
//                    @Override
//                    public void onNext(List<AdapterForce> result) {
//                        Log.e("asd " + result.size() + " -->", "" + result);
//                        Observable.zip(Observable.from(result)
//                                        .take(2)
//                                        .flatMap(new Func1<AdapterForce, Observable<?>>() {
//                                            @Override
//                                            public Observable<?> call(AdapterForce forces) {
//                                                return Observable.just(forces);
//                                            }
//                                        }),
//                                Observable.interval(1000, TimeUnit.MILLISECONDS),
//                                new Func2<Object, Long, AdapterForce>() {
//                                    @Override
//                                    public AdapterForce call(Object o, Long aLong) {
//                                        return (AdapterForce) o;
//                                    }
//                                }).subscribe(new Observer<AdapterForce>() {
//                            @Override
//                            public void onCompleted() {
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                            }
//
//                            @Override
//                            public void onNext(AdapterForce o) {
//                                Log.e("observable", "time: " + System.currentTimeMillis() + " -->> " + o);
//                                getNeighbourhoods(o.getId());
//                            }
//                        });
//                    }
//                });
        Observable<List<Force>> categDetResponseObservable = (Observable<List<Force>>)
                ConnectionService.getPreparedObservable(apiService.getForces(), Force.class, true, true);
        ConnectionService.sub.add(
                categDetResponseObservable.subscribe(new Observer<List<Force>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("errrrrror", "" + e);

                    }
                    @Override
                    public void onNext(List<Force> result) {
                        //cupboard().withDatabase(MainActivity.db).put(Category.class,result);
                        Observable.zip(Observable.from(result)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .take(2)
                                        .flatMap(new Func1<Force, Observable<?>>() {
                                            @Override
                                            public Observable<?> call(Force forces) {
                                                return Observable.just(forces);
                                            }
                                        }),
                                Observable.interval(1000, TimeUnit.MILLISECONDS),
                                new Func2<Object, Long, Force>() {
                                    @Override
                                    public Force call(Object o, Long aLong) {
                                        return (Force) o;
                                    }
                                }).subscribe(new Observer<Force>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onNext(Force o) {
                                Log.e("observable", "time: " + System.currentTimeMillis() + " -->> " + o);
                                getNeighbourhoods(o.getId());
                            }
                        });
                    }
                })
        );
    }

    @Override
    public void getNeighbourhoods(final String force) {
        apiService.getNeighbourhoods(baseUrl + force + "/neighbourhoods")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Neighbourhoods>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("errrrrror", "" + e);
                    }

                    @Override
                    public void onNext(List<Neighbourhoods> result) {
                        // Log.e("neighbourhoods", "" + result);
                        Observable.zip(Observable.from(result)
                                        .flatMap(new Func1<Neighbourhoods, Observable<?>>() {
                                            @Override
                                            public Observable<?> call(Neighbourhoods neighbourhoods) {
                                                return Observable.just(neighbourhoods);
                                            }
                                        }),
                                Observable.interval(1000, TimeUnit.MILLISECONDS),
                                new Func2<Object, Long, Neighbourhoods>() {
                                    @Override
                                    public Neighbourhoods call(Object o, Long aLong) {
                                        return (Neighbourhoods) o;
                                    }
                                }).subscribe(new Observer<Neighbourhoods>() {
                            @Override
                            public void onCompleted() {
                            }

                            @Override
                            public void onError(Throwable e) {
                            }

                            @Override
                            public void onNext(Neighbourhoods o) {
                                Log.e("observableNeigh", "time: " + System.currentTimeMillis() + " -->> " + o);
                                getNeighbourhoodBorders(force, o.getId());
                            }
                        });
                    }
                });
    }

    @Override
    public void getNeighbourhoodBorders(final String force, final String neighbourhood) {
//


//        Observable<List<LatLng>> categDetResponseObservable = (Observable<List<LatLng>>)
//                ConnectionService.getPreparedObservable(apiService.getNeighbourhoodBorder(baseUrl + force + "/" + neighbourhood + "/boundary"), AdapterForce.class, false, true);
//        ConnectionService.sub.add(categDetResponseObservable
        apiService.getNeighbourhoodBorder(force, neighbourhood)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<LatLng>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("errrrrror", "" + e);
                    }

                    @Override
                    public void onNext(List<LatLng> result) {
                        // Log.e("sff",""+result.getFood().getServings().getServing().get(0).getCarbohydrate());
                        //Log.e("boundaries", "" + result);
                        //googleMap.addMarker(new MarkerOptions().position(result.get(0)).title(neighbourhood));
//                        Observable.from(result)
//
//                                .filter(new Func1<LatLng, Boolean>() {
//                                    @Override
//                                    public Boolean call(LatLng latLng) {
//
//                                        return null;
//                                    }
//                                });
                        ctx.updateMap(result, neighbourhood, true);
                    }

                });
        //);

    }



    private Context getContext(){
        return fragmentActivity.getApplicationContext();
    }

    public void getNeighbourhoodByLocation(final LatLng location) {
        if (tick<=0) {
            subscription.add(apiService.getNeighbourhoodByLocation(baseUrl + "locate-neighbourhood?q=" + String.valueOf(location.latitude + Double.valueOf(sweep_x)) + "," + String.valueOf(location.longitude + Double.valueOf(sweep_y)))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onBackpressureBuffer()
                    .subscribe(new Observer<LocatedNeigh>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            if (loadingView.getAlpha()>0){
                                loadingView.setAlpha(loadingView.getAlpha()-0.1f);
                            }
                            Log.e("ERROR!", String.valueOf(e));
                            if (!String.valueOf(e).equals("retrofit2.adapter.rxjava.HttpException: HTTP 429")){
                                sweepCell=sweepSize;
                                getNeighbourhoodByLocation(new LatLng(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude));
                                //pointSweep.run();
                            }
                            else{
                                sweepCell=sweepPrecision;
                                sweep_x=0;
                                sweep_y=0;
                                getNeighbourhoodByLocation(new LatLng(googleMap.getCameraPosition().target.latitude, googleMap.getCameraPosition().target.longitude));
                            }
                            subscription.clear();
                            subscription.remove(subscription);
                        }

                        @Override
                        public void onNext(LocatedNeigh locatedNeigh) {
                            if (!neighs.contains(locatedNeigh)) {
                                getNeighbourhoodBorders(locatedNeigh.getForce(), locatedNeigh.getNeighbourhood());
                                neighs.add(locatedNeigh);
                            }
                            if (loadingView.getAlpha()<1){
                                loadingView.setAlpha(1);
                            }
                            Log.i("data", "received"+" "+String.valueOf(locatedNeigh.getNeighbourhood()));
                            lastNeigh=String.valueOf(locatedNeigh.getNeighbourhood());
                            if (((MapFragment)ctx).isResumed())
                            pointSweep.run();
                        }
                    }));
        }
        pointSweep.setPriority(10);


    }

    public String getCrimeColour(List<LatLng> latLngList){
        String result = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeLowest));
        getCrimeCount(latLngList);
        return result;
    }

    private void getCrimeCount(final List<LatLng> latLngList) {

            final List<LatLng> optimisedLatLng = latLngList;

            String processedLatLng = "";
            for (int i = 0; i < optimisedLatLng.size(); i += 8) {
                processedLatLng += (String.valueOf(optimisedLatLng.get(i).latitude).substring(0, 6) + "," + String.valueOf(optimisedLatLng.get(i).longitude).substring(0, 6));
                if (i < optimisedLatLng.size() - 7) {
                    processedLatLng += ":";
                }
            }
        foundPoly = false;
        Log.i("Processing", "Started"+" "+polygonCount);
        Boolean foundPoly = false;
        if (polygonCount>10) {
            for (int i = polygonCount - 10; i < polygonCount + 10; i += 1) {
                if (polygons[i] != null) {
                    if (polygons[i].getPoints().get(1) == optimisedLatLng.get(1)) {
                        foundPoly = true;
                    }
                }
            }
        }

        Log.i("Processing", "ended");


        if (!foundPoly) {
            Log.i("log", baseUrl + "crimes-street/all-crime?poly=" + processedLatLng);
            subscription.add(apiService.getCrimes(processedLatLng)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.newThread())
                    .subscribe(new Observer<List<CrimeList>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            //getCrimeCount(latLngList);
                            Log.e("Crime Colour Error!", String.valueOf(e));
                            subscription.clear();
                            subscription.remove(subscription);
                            pointSweep.run();
                        }

                        @Override
                        public void onNext(List<CrimeList> crimeList) {
                            if (crimeList.size() != 0) {
                                crimes = crimeList.size();
                                String colour = "000000";
                                if ((crimes > 0) && (crimes < 50)) {
                                    //Lowest crimes
                                    colour = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeLowest));
                                }
                                if ((crimes >= 50) && (crimes < 100)) {
                                    //Low crimes
                                    colour = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeLow));
                                }
                                if ((crimes >= 100) && (crimes < 200)) {
                                    //Medium crimes
                                    colour = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeMedium));
                                }
                                if ((crimes >= 200) && (crimes < 350)) {
                                    //High crimes
                                    colour = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeHigh));
                                }
                                if (crimes >= 350) {
                                    //Lowest crimes
                                    colour = String.valueOf(fragmentActivity.getResources().getColor(R.color.crimeHighest));
                                }

                                PolygonOptions poly = new PolygonOptions()
                                        .addAll(optimisedLatLng)
                                        .strokeWidth(0)
                                        .fillColor(Integer.valueOf(colour));
                                polygonCount++;
                                polygons[polygonCount] = googleMap.addPolygon(poly);
                                if (polygonCount == 256) {
                                    for (int i = 257; i <= 512; i++) {
                                        polygons[i].remove();
                                    }
                                }
                                if (polygonCount == 512) {
                                    polygonCount = 1;
                                    for (int i = 1; i <= 256; i++) {
                                        polygons[i].remove();
                                    }
                                }
                                //googleMap.addMarker(new MarkerOptions().title(forceName).position(result.get(0)).icon(getMarkerIcon(Integer.valueOf(colour))));
                            }
                        }
                    }));
        }
    }

    @Override
    public void getNeighbourhoodDetails(String force, String neighbourhood) {
        apiService.getNeighbourhoodDetails(force, neighbourhood).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Neighbourhoods>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Neighbourhoods neighbourhoods) {
                        ctx.addMarker(neighbourhoods);
                    }
                });
    }
}

