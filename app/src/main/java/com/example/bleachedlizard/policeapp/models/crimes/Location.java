package com.example.bleachedlizard.policeapp.models.crimes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by PC-TAE on 01/12/2016.
 */

public class Location {

    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("street")
    @Expose
    private Street street;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The street
     */
    public Street getStreet() {
        return street;
    }

    /**
     *
     * @param street
     * The street
     */
    public void setStreet(Street street) {
        this.street = street;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
