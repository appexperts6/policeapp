package com.example.bleachedlizard.policeapp.mainactivity;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.AppCompatImageView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.bleachedlizard.policeapp.BaseActivity;
import com.example.bleachedlizard.policeapp.R;
import com.example.bleachedlizard.policeapp.additionalinfofragment.AdditionalInfoFragment;
import com.example.bleachedlizard.policeapp.mapfragment.MapFragment;
import com.example.bleachedlizard.policeapp.mvp.BasePresenterInterface;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.bleachedlizard.policeapp.Constants.REQUEST_INVITE;

public class MainActivity extends BaseActivity implements MainActivityContract.ViewContract,
        com.example.bleachedlizard.policeapp.mapfragment.MapFragment.OnFragmentInteractionListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Nullable @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @Nullable @BindView(R.id.content_main)
    SlidingUpPanelLayout slidingUpPanelLayout;

    public MapFragment mapFragment;
    private Double[] latLong = new Double[2];


    private MainActivityContract.PresenterContract presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initialisePresenter();
        mapFragment = new com.example.bleachedlizard.policeapp.mapfragment.MapFragment();
        createFragment(mapFragment);

    }

    @Override
    protected void onResume() {
        super.onResume();
        initialiseUiElements();
    }

    public void createFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fm.popBackStack();
        fm.popBackStackImmediate();
        ft.replace(R.id.fragment_holder_framelayout, fragment);
        ft.commit();
    }

    private void initialiseUiElements() {

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        DisplayMetrics metrics = new DisplayMetrics();
        double density = getResources().getDisplayMetrics().density;
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ActionBarDrawerToggle toggle;
        if (metrics.widthPixels / density > 620) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        else {

            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
             toggle.syncState();
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void initialisePresenter() {
        presenter = new MainActivityPresenter(this);
        presenter.start();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

   /* private Drawable resize(Drawable drawable, int height, int width) {
        Bitmap b = ((BitmapDrawable)drawable).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, height, width, false);
        return new BitmapDrawable(getResources(), bitmapResized);

    }*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        MenuItem locationItem = menu.findItem(R.id.action_location);
        MenuItem inviteItem = menu.findItem(R.id.action_invite);
        SearchView searchView = (SearchView) searchItem.getActionView();

        inviteItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.i("INVITE", "You pressed invite!");
                View dialogView = LayoutInflater.from(MainActivity.this).inflate(R.layout.email_dialog, null);
                final EditText nameText = (EditText)dialogView.findViewById(R.id.name_text);
                AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).setView(dialogView)
                        .setTitle("Share PoliceApp")
                        .setMessage("Enter the name of your recipient(s)")
                        .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                presenter.invite(nameText.getText().toString());
                                dialog.dismiss();
                            }
                        }).setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialog.show();
                return false;
            }
        });


        ((LinearLayout) searchView.getChildAt(0)).setGravity(Gravity.BOTTOM);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Search for the address
                presenter.getThisLocation(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_location)
            presenter.goToCurrentLocation(mapFragment);
        //Call method to find location
        //presenter.getMyAddress();
        return super.onOptionsItemSelected(item);
    }


    /*@Override
    public void setPresenter(BasePresenterInterface presenter) {

    }*/


    public void setNavDrawerAdapter(NavDrawerAdapter adapater) {
        recyclerView.setAdapter(adapater);
    }

    @Override
    public void setSearchedLoc(Double[] loc){
        //elemt 0 is lat, element 1 is long
        latLong =loc;
        mapFragment.zoomToLocation(latLong[0],latLong[1]);
    }



    /*public void viewCrimes(View view) {
        presenter.viewCrimes();
        Log.i("Pressed", "View crimes");
        com.example.bleachedlizard.policeapp.mapfragment.MapFragment mapFragment = new com.example.bleachedlizard.policeapp.mapfragment.MapFragment();
        createFragment(mapFragment);

        drawer.closeDrawer(GravityCompat.START);
    }*/


    public void viewContactInformation(String forceId, String neighbourhoodId) {
        Fragment fragment = AdditionalInfoFragment.newInstance(forceId, neighbourhoodId);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fm.popBackStack();
        ft.replace(R.id.contact_details_frame, fragment);
        ft.commit();

        drawer.closeDrawer(GravityCompat.START);
        if (slidingUpPanelLayout != null)
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void setPresenter(BasePresenterInterface presenter) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_INVITE) {
            if (resultCode == RESULT_OK)
                Toast.makeText(this, "Sent!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Failed...", Toast.LENGTH_SHORT).show();
        }
    }
}
