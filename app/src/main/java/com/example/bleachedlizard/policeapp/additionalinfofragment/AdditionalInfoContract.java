package com.example.bleachedlizard.policeapp.additionalinfofragment;

import com.example.bleachedlizard.policeapp.mvp.BasePresenterInterface;
import com.example.bleachedlizard.policeapp.mvp.BaseViewInterface;

/**
 * Created by bleac on 21/11/2016.
 */

public interface AdditionalInfoContract {

    interface PresenterContract extends BasePresenterInterface{
        void getContactDetailsList(String forceId, String neighbourhoodId);
    }

    interface ViewContract extends BaseViewInterface<PresenterContract>{
        void setContactDetailsAdapter(AdditionalInfoAdapter adapter);
        void displayError(String message);
        void dismissProgressDialog();
    }
}
