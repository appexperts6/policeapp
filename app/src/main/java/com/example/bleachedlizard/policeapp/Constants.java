package com.example.bleachedlizard.policeapp;

/**
 * Created by bleac on 21/11/2016.
 */

public final class Constants {
    public static final String BASE_URL = "https://data.police.uk/api/";
    public static final int SUCCESS_RESULT = 0;

    public static final int FAILURE_RESULT = 1;

    public static final String PACKAGE_NAME =
            "com.example.bleachedlizard.policeapp";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String LOCATION_RECEIVER = PACKAGE_NAME + ".LOCATION_RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final String LOCATION_DATA_ADDRESS = PACKAGE_NAME + ".LOCATION_DATA_ADDRESS";

    public static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 0;
    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 0;
    public static final int REQUEST_INVITE = 2;
}