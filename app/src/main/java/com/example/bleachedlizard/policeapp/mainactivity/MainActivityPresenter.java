package com.example.bleachedlizard.policeapp.mainactivity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.bleachedlizard.policeapp.Constants;
import com.example.bleachedlizard.policeapp.R;
import com.example.bleachedlizard.policeapp.apiservices.ConnectionService;
import com.example.bleachedlizard.policeapp.apiservices.CrimeStatsApi;
import com.example.bleachedlizard.policeapp.mapfragment.MapFragment;
import com.example.bleachedlizard.policeapp.models.Force;
import com.example.bleachedlizard.policeapp.models.Neighbourhoods;
import com.example.bleachedlizard.policeapp.services.FetchAddressService;
import com.example.bleachedlizard.policeapp.services.FetchLocationService;
import com.google.android.gms.appinvite.AppInviteInvitation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.example.bleachedlizard.policeapp.Constants.MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
import static com.example.bleachedlizard.policeapp.Constants.MY_PERMISSIONS_REQUEST_FINE_LOCATION;
import static com.example.bleachedlizard.policeapp.Constants.REQUEST_INVITE;

/**
 * Created by bleac on 21/11/2016.
 */

public class MainActivityPresenter implements MainActivityContract.PresenterContract, NavDrawerAdapter.NeighbourhoodClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private MainActivityContract.ViewContract activity;
    private Context context;
    private AddressResultReceiver mResultReceiver;
    private LocationResultReceiver mLocationReciever;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    private Double[] latLong = new Double[2];
    public CrimeStatsApi api;
    private ArrayList<NavDrawerAdapter.AdapterForce> forcesList;
    private int numberOfForces;


    public MainActivityPresenter(MainActivityContract.ViewContract activity) {
        this.activity = activity;
        if (activity instanceof Activity) {
            context = (Context) activity;
        } else {
            // TODO We'll figure something out.
        }
    }

    @Override
    public void start() {
        mResultReceiver = new AddressResultReceiver(new Handler());
        mLocationReciever = new LocationResultReceiver(new Handler());
        mGoogleApiClient = new GoogleApiClient.Builder(this.context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        connectToApi();
        getForceData();
    }

    private void connectToApi() {
        api = ConnectionService.getClient().create(CrimeStatsApi.class);
    }


    @Override
    public void centreOnNeighbourhood(String forceId, String neighbourhoodId) {

    }

    private void processForceData(List<Force> forces){
        numberOfForces = forces.size();
        forcesList = new ArrayList<>();
        for (int i = 0; i < forces.size(); i++){
            Force force = forces.get(i);
            final NavDrawerAdapter.AdapterForce adapterForce = extractAdapterForce(force);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getNeighbourhoodData(adapterForce);
                }
            }, (i * 60));
        }

    }

    private NavDrawerAdapter.AdapterForce extractAdapterForce(Force force) {

        String forceId = force.getId();
        String forceName = force.getName();

        NavDrawerAdapter.AdapterForce adapterForce = new NavDrawerAdapter.AdapterForce(forceName, forceId, null);
        return adapterForce;
    }

    public void getForceData() {
        Observable<List<Force>> getForcesObservable = (Observable<List<Force>>)
                ConnectionService.getPreparedObservable(api.getForces(), Force.class, false, false);

        getForcesObservable
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Force>>() {
                    @Override
                    public void onCompleted() {
                        Log.i("AdapterForce data", "Completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Force data ERROR!", String.valueOf(e));
                    }

                    @Override
                    public void onNext(List<Force> forces) {
                        if ((forces.size()!=0)&&(forces!=null)){
                            //data received is okay, use it...
                            processForceData(forces);
                        }
                    }
                });
    }

    public void getNeighbourhoodData(final NavDrawerAdapter.AdapterForce adapterForce) {

        Observable<List<Neighbourhoods>> getForcesObservable = (Observable<List<Neighbourhoods>>)
                ConnectionService.getPreparedObservable(api.getNeighbourhoods(adapterForce.forceId), Neighbourhoods.class, false, false);

        getForcesObservable
                .onBackpressureBuffer()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Neighbourhoods>>() {
                    @Override
                    public void onCompleted() {
                        Log.i("AdapterForce data", "Completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Force data ERROR!", String.valueOf(e));
                    }

                    @Override
                    public void onNext(List<Neighbourhoods> neighbourhoods) {
                        if ((neighbourhoods.size()!=0)&&(neighbourhoods!=null)){
                            //data received is okay, use it...
                            processNeighbourhoodData(adapterForce, neighbourhoods);
                        }
                    }
                });
    }


    private void processNeighbourhoodData(NavDrawerAdapter.AdapterForce adapterForce, List<Neighbourhoods> neighbourhoods) {
        ArrayList<NavDrawerAdapter.Neighbourhood> adapterNeighbourhoods = new ArrayList<>();
        for (Neighbourhoods neighbourhood : neighbourhoods){
            String id = neighbourhood.getId();
            String name = neighbourhood.getName();
            NavDrawerAdapter.Neighbourhood adapterNeighbourhood = new NavDrawerAdapter.Neighbourhood(name, id);
            adapterNeighbourhoods.add(adapterNeighbourhood);
        }
        adapterForce.neighbourhoodList = adapterNeighbourhoods;
        forcesList.add(adapterForce);
        if (forcesList.size() == numberOfForces){
            createAdapter();
        }
    }

    private void createAdapter() {
        NavDrawerAdapter adapter = new NavDrawerAdapter(context, forcesList, this);
        activity.setNavDrawerAdapter(adapter);
    }

    @Override
    public void onNeighbourhoodClick(String forceId, String neighbourhoodId) {
        activity.viewContactInformation(forceId, neighbourhoodId);
    }

    @Override
    public void getMyAddress() {
        startIntentService();
    }

    /**
     * This starts an intent service that returns the postcode of the current location
     */

    public void goToCurrentLocation(MapFragment mapFragment) {

        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        } else {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            mapFragment.zoomToLocation(location.getLatitude(), location.getLongitude());
        }
    }


    protected void startIntentService() {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this.context, FetchAddressService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        context.startService(intent);
    }


    @Override
    public void getThisLocation(String query) {
        //starts the chain reaction
        startSecondIntentService(query);
    }

    public void sendThisLocation() {
        //ends the chain reaction
        activity.setSearchedLoc(latLong);
    }

    /**
     * This starts an intent service that returns the location for a given search()
     */
    protected void startSecondIntentService(String search) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this.context, FetchLocationService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(Constants.LOCATION_RECEIVER, mLocationReciever);

        // Pass the location data as an extra to the service.
        intent.putExtra(Constants.LOCATION_DATA_ADDRESS, search);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        context.startService(intent);
    }

    /**
     * This happens when the Google com.example.bleachedlizard.policeapp.models.crimes.Location API is connected
     *
     * @param bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this.context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //This means permissions weren't granted
            //Fix it
            Log.e("PERMISSIONS", "NOT GRANTED!");
            //ASK FOR PERMISSIONS! With marshmallow+ you need to ask for permissions at runtime.
            ActivityCompat.requestPermissions((Activity) this.context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
            ActivityCompat.requestPermissions((Activity) this.context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            //Do something with com.example.bleachedlizard.policeapp.models.crimes.Location at this point
            Log.e("LOCATION", "" + mLastLocation.getLatitude() + "  " + mLastLocation.getLongitude());
            // Determine whether a Geocoder is available.
            if (!Geocoder.isPresent()) {
                //No geocodere availible, do something
                return;
            } else {

            }


        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("WAIT", "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("OOPS", "Connection failed: " + connectionResult.getErrorCode());
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            // Display the address string or an error message sent from the intent service.
            if (resultCode == Constants.SUCCESS_RESULT) {
                String address = resultData.getString(Constants.RESULT_DATA_KEY);
                Log.e("HEEEEY", address);

            } else {
                Log.e("DOH", resultData.getString(Constants.RESULT_DATA_KEY));
            }
        }

    }

    class LocationResultReceiver extends ResultReceiver {
        public LocationResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            Log.i("Boop", "The com.example.bleachedlizard.policeapp.models.crimes.Location");
            // Display the address string or an error message sent from the intent service.
            if (resultCode == Constants.SUCCESS_RESULT) {
                //the first 2 elemts of this list are the latidue and longitude respectively
                ArrayList<String> address = resultData.getStringArrayList(Constants.RESULT_DATA_KEY);
                if (address.isEmpty())
                    Log.e("Problem", "Did not return coords from intentService");
                else {
                    latLong[0] = Double.parseDouble(address.get(0));
                    latLong[1] = Double.parseDouble(address.get(1));
                    sendThisLocation();
                    Log.e("WHERE", address.get(0) + " " + address.get(1));
                }

            } else {
                Log.e("Problem", "Did not return coords from intentService");
            }


        }

    }
    @Override
    public void invite(String name){
        //invite code here
        Log.i("INVITE", "You pressed invite!");
                        Intent intent = new AppInviteInvitation.IntentBuilder("Share This App")
                                .setMessage("Dear " + name + ", \nWe would like your thoughts on this app below.")
                                .setDeepLink(Uri.parse("http://example.com/bleachedlizard/policeapp"))
                                .setEmailHtmlContent(context.getString(R.string.invitation_email_html_content))
                                .setEmailSubject("PoliceApp")
                                .build();
                        ((MainActivity)context).startActivityForResult(intent, REQUEST_INVITE);


    }


}
