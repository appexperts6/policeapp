package com.example.bleachedlizard.policeapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by bleac on 21/11/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {



    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "policeApp.db";

    private static DatabaseHelper databaseHelper;



    public DatabaseHelper(Context context){

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }



    public static DatabaseHelper getInstance(Context context){

        if (databaseHelper == null){

            databaseHelper = new DatabaseHelper(context);

        }

        return databaseHelper;

    }



    static {

        cupboard().register(Crime.class);



    }



    @Override

    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        cupboard().withDatabase(sqLiteDatabase).createTables();

    }



    @Override

    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        cupboard().withDatabase(sqLiteDatabase).upgradeTables();



    }

}
