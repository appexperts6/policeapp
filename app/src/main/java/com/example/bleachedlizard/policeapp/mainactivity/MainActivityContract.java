package com.example.bleachedlizard.policeapp.mainactivity;

import com.example.bleachedlizard.policeapp.mapfragment.MapFragment;
import com.example.bleachedlizard.policeapp.mvp.BasePresenterInterface;
import com.example.bleachedlizard.policeapp.mvp.BaseViewInterface;

/**
 * Created by bleac on 21/11/2016.
 */

public interface MainActivityContract {

    interface PresenterContract extends BasePresenterInterface{
        void getMyAddress();
        void goToCurrentLocation(MapFragment mapFragment);
        void getThisLocation(String query);
        void centreOnNeighbourhood(String forceId, String neighbourhoodId);
        void invite(String name);
    }

    interface ViewContract extends BaseViewInterface<BasePresenterInterface>{
        void viewContactInformation(String forceId, String neighbourhoodId);
        void setNavDrawerAdapter(NavDrawerAdapter adapter);
        void setSearchedLoc(Double[]loc);
    }
}
