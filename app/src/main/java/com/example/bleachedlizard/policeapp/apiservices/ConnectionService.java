package com.example.bleachedlizard.policeapp.apiservices;

/**
 * Created by bleac on 21/11/2016.
 */






        import android.support.v4.util.LruCache;
        import android.util.Log;

        import com.google.gson.Gson;
        import com.google.gson.GsonBuilder;

        import java.io.IOException;

        import okhttp3.Interceptor;
        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import okhttp3.Response;
        import okhttp3.logging.HttpLoggingInterceptor;
        import retrofit2.Retrofit;
        import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
        import retrofit2.converter.gson.GsonConverterFactory;
        import rx.Observable;
        import rx.android.schedulers.AndroidSchedulers;
        import rx.schedulers.Schedulers;
        import rx.subscriptions.CompositeSubscription;

/**
 * Created by TAE on 27/10/2016.
 */


public class ConnectionService {

    //public static final String BASE_URL = "https://dl.dropboxusercontent.com/u/1559445/ASOS/SampleApi/";
    public static final String BASE_URL = "https://data.police.uk/api/";
    private static Retrofit retrofit = null;

    public static CompositeSubscription sub = new CompositeSubscription();


    private static LruCache<Class<?>, Observable<?>> apiObservables;

    private static OkHttpClient okHttpClient;

    public static Retrofit getClient() {
        //interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        okHttpClient = buildClient();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit==null) {
             apiObservables = new LruCache<>(1);
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    //.addConverterFactory(GsonConverterFactory.create())

                    .addConverterFactory(GsonConverterFactory.create(gson))
                    //.client(okHttpClient)
                    //.client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())

                    .build();
        }
        return retrofit;
    }



    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     * @return
     */
    public static OkHttpClient buildClient(){

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        //Log.e("respssss","jhfgjfghkghlhl");

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Log.e("resp","chain"+chain.toString());
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..


                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                Log.e("resp2","sdgsfh");
                return chain.proceed(request);
            }
        });

        return  builder.build();
    }
    /**
     * Method to clear the entire cache of observables
     */
    public void clearCache(){
        apiObservables.evictAll();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param clazz
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public static Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache){

        Observable<?> preparedObservable = null;

        if(useCache)//this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);
        Log.e("using cache","!!!!!!!!!!!");
        if(preparedObservable!=null)
            return preparedObservable;



        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if(cacheObservable){
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }
        Log.e("using not cache",">>>>>>>>>>>>>>>!!!!!!");

        return preparedObservable;
    }


}
