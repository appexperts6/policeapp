package com.example.bleachedlizard.policeapp.models;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocatedNeigh{

@SerializedName("force")
@Expose
private String force;
@SerializedName("neighbourhood")
@Expose
private String neighbourhood;

/**
* 
* @return
* The force
*/
public String getForce() {
return force;
}

/**
* 
* @param force
* The force
*/
public void setForce(String force) {
this.force = force;
}

/**
* 
* @return
* The neighbourhood
*/
public String getNeighbourhood() {
return neighbourhood;
}

/**
* 
* @param neighbourhood
* The neighbourhood
*/
public void setNeighbourhood(String neighbourhood) {
this.neighbourhood = neighbourhood;
}

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof LocatedNeigh))
            return false;
        LocatedNeigh neigh = (LocatedNeigh)obj;
        return this.force.equals(neigh.force) && this.neighbourhood.equals(neigh.neighbourhood);
    }
}
