package com.example.bleachedlizard.policeapp.mapfragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bleachedlizard.policeapp.R;
import com.example.bleachedlizard.policeapp.customviews.LoadingView;
import com.example.bleachedlizard.policeapp.models.CountyCoords;
import com.example.bleachedlizard.policeapp.models.Neighbourhoods;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.http.HEAD;

import static android.text.Html.fromHtml;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements MapContract.ViewContract, OnMapReadyCallback {



    private OnFragmentInteractionListener mListener;
    private GoogleMap googleMap;
    private boolean mapReady;
    @BindView(R.id.mapView)
    MapView mapView;

    @BindView(R.id.loadingView)
    LoadingView loadingView;


    private MapContract.PresenterContract mPresenter;
    private MapPresenter mapPresenter;
    private Context context;
    Unbinder butterknifeUnbinder;
    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance(String param1, String param2) {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.map_fragment, container, false);
        butterknifeUnbinder = ButterKnife.bind(this,v);

        //Init the google map
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        mapPresenter = new MapPresenter(this, getActivity());
        if (mapReady)
            mapPresenter.googleMap = this.googleMap;


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapPresenter.start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("fragment","detaching");
        mListener = null;
        mapPresenter = null;
        mPresenter=null;
        mapView=null;
        if (googleMap != null)
        googleMap.clear();
        butterknifeUnbinder.unbind();
    }



    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (mapPresenter != null)
            mapPresenter.googleMap = googleMap;
        mapReady = true;
       // mPresenter.getForces();
        getJSONForcesAsync();


        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            private float currentZoom = -1;

            @Override
            public void onCameraChange(CameraPosition pos) {
                Log.e("camerachange","location: "+googleMap.getCameraPosition().target);
                mapPresenter.sweepinit=false;
                mapPresenter.sweepComplete=false;
                if(currentZoom>10) {
                    mapPresenter.getNeighbourhoodByLocation(googleMap.getCameraPosition().target);
                }
                if (pos.zoom != currentZoom){
                    currentZoom = pos.zoom;
                    // do you action here
                    Log.e("zoom change", "to: "+currentZoom);

                }
            }
        });
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(53.5,-1.5),6.5f));


    }


    public void setPresenter(@NonNull MapContract.PresenterContract presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void updateMap(List<LatLng>result, String forceName ,Boolean isNeighbourhood) {
        String colour = String.valueOf(getResources().getColor(R.color.force));
        if (isNeighbourhood){
            colour = mapPresenter.getCrimeColour(result);
        }
        else {
            PolygonOptions poly = new PolygonOptions()
                    .addAll(result)
                    .strokeWidth(4f)
                    .strokeColor(Integer.valueOf(colour));
            googleMap.addPolygon(poly);
            //googleMap.addMarker(new MarkerOptions().title(forceName).position(result.get(0)).icon(getMarkerIcon(Integer.valueOf(colour))));
        }



    }
    //shawn's code: MapFragment
    private void addMarker(String force, LatLng placeholder) {
        MarkerOptions marker = new MarkerOptions().title(force).position(placeholder);
        googleMap.addMarker(marker);
    }
    //shawn's code
    public void addMarker(Neighbourhoods neighbourhoods) {
        LatLng centre = new LatLng(neighbourhoods.getCentre().getLatitude(), neighbourhoods.getCentre().getLongitude());
        MarkerOptions marker = new MarkerOptions().title(fromHtml(neighbourhoods.getName()).toString()).position(centre);
        googleMap.addMarker(marker);
    }
    @Override
    public void zoomToLocation(double lat, double lng) {
        LatLng coordinate = new LatLng(lat, lng);
        if (googleMap != null)
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 10.1f));
        mapPresenter.getNeighbourhoodByLocation(new LatLng(lat,lng));
    }

    public BitmapDescriptor getMarkerIcon(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void getJSONForcesAsync() {

        String[] forces = new String[0];
        try {
            forces = ((Activity)context).getAssets().list("policeForces");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for(final String f1 : forces){
            new AsyncTask<String, Void, List<LatLng>>(){
                @Override
                protected List<LatLng> doInBackground(String... force) {
                    Log.v("names",force[0]);
                    Gson gson = new Gson();
                    CountyCoords coords;
                    coords =  gson.fromJson(loadJSONFromAsset("policeForces/"+f1), CountyCoords.class);
                    Log.e("json", ""+coords.getFeatures().get(0).getGeometry().getCoordinates().get(0).get(0));
                    List<LatLng> listLatLng = new ArrayList<>();
                    for (int i=0;i<coords.getFeatures().get(0).getGeometry().getCoordinates().get(0).size();i++) {
                        listLatLng.add(new LatLng( coords.getFeatures().get(0).getGeometry().getCoordinates().get(0).get(i).get(1),
                                coords.getFeatures().get(0).getGeometry().getCoordinates().get(0).get(i).get(0)));
                        //Log.e("coords",""+listLatLng.get(i));
                    }
                    return listLatLng;
                }

                @Override
                protected void onPostExecute(List<LatLng> listLatLng) {
                    if (MapFragment.this.isAdded())
                    updateMap(listLatLng, f1, false);
                }
            }.execute(f1);

            //addMarker(f1, listLatLng.get(0));
        }



    }

    public String loadJSONFromAsset(String countyName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(countyName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}
