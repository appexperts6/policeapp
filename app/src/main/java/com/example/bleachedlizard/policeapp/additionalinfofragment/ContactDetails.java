package com.example.bleachedlizard.policeapp.additionalinfofragment;

/**
 * Created by bleac on 23/11/2016.
 */

public class ContactDetails {

    public String name;
    public String phone;
    public String email;
    public String bio;
}
